/* 
 * \file traj.c 
 * \brief the main trajectory tracking routines
 *
 * \author Michael Kantner
 *
 * Trajectories are optimized 1-d lookup tables.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <stdlib.h>		/* For size_t */
#include "traj.h"

/*
#define TRAJ_DEBUG 
*/

/*
 * Create the traj data structure,
 * initialize all elements,
 * and return it.
 */

TRAJ_DATA *traj_create(void){
  TRAJ_DATA *t = NULL;

  t = (TRAJ_DATA *) malloc(sizeof(TRAJ_DATA));
  if (t==NULL) return NULL;

  t->time = NULL;
  t->data = NULL;

  t->current = -1;
  t->nrows   = 0;
  t->ncols   = 0;
  t->flag    = 0;

  return t;
}

int traj_resize(TRAJ_DATA *t, int rows, int cols){
  int i;

  if (t==NULL) return (-1);

  /* Malloc the time vector */
  t->time = (double  *) malloc((size_t) rows*sizeof(double));
  if (t->time == NULL) return (-1);

  /* Malloc the pointers to the data */
  t->data = (double **) malloc((size_t) rows*sizeof(double *));
  if (t->data == NULL) {
    free (t->time);
    return (-1);
  }

  /* Malloc the data vectors */
  for (i = 0; i < rows; i++){
    t->data[i] = (double  *) malloc((size_t) cols*sizeof(double));
    if (t->data[i] == NULL){
      int j;

      for (j=0; j < i; j++) free (t->data[j]);
      free (t->data);
      free (t->time);
      return (-1);
    }
  }

  t->current = 0;          /* initially at 0           */
  t->nrows = (int) rows;
  t->ncols = (int) cols;
  t->flag = 0;             /* no flags set initially   */

  return 0;
}

TRAJ_DATA  *traj_init(int rows, int cols) {
  TRAJ_DATA *handle;

  handle = traj_create();

  if (traj_resize(handle,rows,cols)<0){
    printf("  Matrix initial sizing error!\n");
    traj_free(handle);
    return NULL;
  }

  return handle;
}


TRAJ_DATA *traj_load(char *traj_file) {
  FILE *trjfp;
  double nrows, ncols;	/* The size of the trajectory matrix */
  double t;
  double *tempdata;
  int i,j;
  TRAJ_DATA *trjp = NULL;

#ifdef TRAJ_DEBUG
  printf("traj_load:  Starting to load the trajectory\n");
#endif

  if ((trjfp = fopen(traj_file, "r")) == NULL) { /* If the does not exist */
    return NULL;
  }

#ifdef TRAJ_DEBUG
  printf("traj_load:  Opened the trajectory file\n");
#endif

  if (fscanf(trjfp, "%lf %lf",&nrows,&ncols) == EOF) {
    fclose(trjfp);
    return NULL;  /* Some type of error message */
  }

  while ( getc(trjfp) != '\n') ; /*read until end of line*/

#ifdef TRAJ_DEBUG
  printf("\n  Rows: %g, Cols: %g \n",nrows,ncols);
#endif

  if ( (nrows < 2) || (ncols < 1) ) return NULL; /* Need some data */

  /* At this point, the file seems OK, so read it */
    
  trjp = traj_create();
  if (trjp == NULL) {
    fclose(trjfp);
    return NULL;
  }

  if (traj_resize(trjp,nrows,ncols) < 0){
#ifdef TRAJ_DEBUG
    printf("traj_load:  Error allocating trajectory space\n");
#endif
    fclose(trjfp);
    traj_free(trjp);
    return NULL;
  }

  for (i = 0; i < nrows; i++){
    fscanf(trjfp, "%lf", &t);  /* Read in the time value */
    trjp->time[i]=t;
      
    tempdata = trjp->data[i];
    for (j = 0; j < ncols; j++){
      fscanf(trjfp, "%lf", tempdata+j );
    }
    while ( getc(trjfp) != '\n') ; /*read until end of line*/

  }


#ifdef TRAJ_DEBUG
  printf("traj_load:  Loaded the trajectory file\n");
#endif

  fclose(trjfp);

#ifdef TRAJ_DEBUG
  printf("traj_load  Closed the trajectory file.\n");
#endif

  /* Check that the time vector is strictly increasing */
  for (i=0; i<trjp->nrows - 1; i++)
    if (trjp->time[i]>=trjp->time[i+1]) trjp->flag |= TRAJF_BADTIME;


#ifdef TRAJ_DEBUG
  printf("traj_load:  Trajectory Data:\n");
  traj_disp(trjp);
  printf("Press return to continue\n");
  getchar();
#endif

  return trjp;
}


void traj_free(TRAJ_DATA *trjp) {
  if (trjp != NULL) {
    int j;
    
    if (trjp->time != NULL)  free(trjp->time);
    if (trjp->data != NULL){
      for (j=0; j<trjp->nrows; j++)  free(trjp->data[j]);
      free(trjp->data);
    }
    free(trjp);
  }
}

int traj_reset(TRAJ_DATA *trjp) {
  trjp->current = 0;
  return 0;
}


int traj_rowset(TRAJ_DATA *trjp, int row) {
  return traj_setcurrent(trjp, row);
}

int traj_setcurrent(TRAJ_DATA *trjp, int row){
  trjp->current = row;
  return 0;
}

int traj_read(TRAJ_DATA *trjp, double *vector, double destime) { 
  /*
   Returns: -1 on an error
             0 if at or before the start of the matrix
             1 if between time values
	     2 if at or after the end of the matrix
   */

  int j;
  double g1now, g1next, tnow, tnext;
  int retvalue;

  if (trjp->flag & TRAJF_BADTIME) return -1;

  if (destime <= trjp->time[0]){
    retvalue = 0;
    tnow = trjp->time[0];
    tnext = trjp->time[1];
    trjp->current=0;
  }
  else if (destime >= trjp->time[trjp->nrows - 1]){
    retvalue = 2;
    tnow = trjp->time[trjp->nrows - 2];
    tnext = trjp->time[trjp->nrows - 1];
    trjp->current=trjp->nrows-2;
  }
  else{
    /* ASSERT: destime < (tmax=time[trjp->nrows - 1])     */
    retvalue = 1;

    /* Just in case time goes backwards... */
    if (destime<trjp->time[trjp->current]) trjp->current=0;

    /* Get Current Index Times    */
    /* ASSERT: current < nrows -1 */
    tnow = trjp->time[trjp->current];
    tnext = trjp->time[trjp->current+1];

    /* Increment current position till proper time */
    while (destime > tnext) {   
      (trjp->current)++;
      tnow = tnext;
      tnext = trjp->time[trjp->current+1];
    }
  }

  /* Perform the linear interpolation */
  for (j=0; j<trjp->ncols; j++){
    g1now  = *(trjp->data[trjp->current] + j);
    g1next = *(trjp->data[trjp->current+1]+j);
    vector[j] = g1now + (destime-tnow)*(g1next-g1now)/(tnext-tnow);
  }

  return retvalue;
}  

int traj_rate(TRAJ_DATA *trjp, double *vector, double destime) { 
  /*
   Returns: -1 on an error
             0 if at or before the start of the matrix
             1 if between time values
	     2 if at or after the end of the matrix
   */

  int j;
  double g1now, g1next, tnow, tnext;
  int retvalue;

  if (trjp->flag & TRAJF_BADTIME) return -1;

  if (destime <= trjp->time[0]){
    retvalue = 0;
    tnow = trjp->time[0];
    tnext = trjp->time[1];
    trjp->current=0;
  }
  else if (destime >= trjp->time[trjp->nrows - 1]){
    retvalue = 2;
    tnow = trjp->time[trjp->nrows - 2];
    tnext = trjp->time[trjp->nrows - 1];
    trjp->current=trjp->nrows-2;
  }
  else{
    /* ASSERT: destime < (tmax=time[trjp->nrows - 1])     */
    retvalue = 1;

    /* Just in case time goes backwards... */
    if (destime<trjp->time[trjp->current]) trjp->current=0;

    /* Get Current Index Times    */
    /* ASSERT: current < nrows -1 */
    tnow = trjp->time[trjp->current];
    tnext = trjp->time[trjp->current+1];

    /* Increment current position till proper time */
    while (destime > tnext) {   
      (trjp->current)++;
      tnow = tnext;
      tnext = trjp->time[trjp->current+1];
    }
  }

  /* Perform the linear interpolation */
  for (j=0; j<trjp->ncols; j++){
    g1now  = *(trjp->data[trjp->current] + j);
    g1next = *(trjp->data[trjp->current+1]+j);
    vector[j] = (g1next-g1now)/(tnext-tnow);
  }

  return retvalue;
}  

void traj_disp(TRAJ_DATA *trjp)
{
  int i,j;

  for (i=0; i<trjp->nrows; i++){
    printf("Row: %4i, Time %9.3f:  ",i,trjp->time[i]);
    for (j=0; j< trjp->ncols; j++){
      printf("%9.3f  ",*(trjp->data[i]+j));
    }
    printf("\n");
  }

}

int traj_outputs(TRAJ_DATA *trjp){
  return trjp->ncols;
}

int traj_numcols(TRAJ_DATA *trjp){
  return traj_outputs(trjp);
}

int traj_rows(TRAJ_DATA *t){
  return t->nrows;
}

int traj_numrows(TRAJ_DATA *trjp){
  return traj_rows(trjp);
}

int traj_current(TRAJ_DATA *trjp){
  return trjp->current;
}

int traj_getcurrent(TRAJ_DATA *trjp){
  return traj_current(trjp);
}

int traj_status(TRAJ_DATA *trjp){
  return trjp->flag;
}
