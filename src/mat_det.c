/*
 * \file mat_det.c 
 * \brief general purpose matrix routines
 *
 * \date August 1995
 * \author Michael Kantner
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include "matrix.h"

int mat_det(double *det, MATRIX *mat){

  if (mat == (MATRIX *)0) return -1;
  if (mat->ncols != mat->nrows) return -1;

  *det=mat_det_f(mat);

  return 0;
}

double mat_det_f(MATRIX *mat){

  register int i,j,k;
  register int rows = mat_get_rows(mat);
  register int sign = 1;
  register double det = 0;
  MATRIX *minor;

  if (rows==1) 
    return mat->real[0];
  if (rows==2)
    return (mat->real[0]*mat->real[3] - mat->real[1]*mat->real[2]);

  minor = mat_init(rows-1,rows-1);
  
  for (i=0; i<rows; i++){
    int cnt=0;

    for (j=1; j<rows; j++)      /* We skip the first column always */
      for (k=0; k<rows; k++){   /* Skip the appropriate row of stuff */
	if (k==i) continue;
	minor->real[cnt] = mat->real[j*rows+k];
	cnt++;
      }
    det = det + sign*mat->real[i]*mat_det_f(minor);
    sign *= -1;
  }

  mat_free(minor);
  return det;
}
