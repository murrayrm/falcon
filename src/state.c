/*
 * \file state.c
 * \brief State space controller implementation
 *
 * \author Michael Kantner
 *
 * Basic implementation of a state space controller.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h> 
#include <string.h>
#include "state.h"

/*
#define SDB
*/

STATE_SPACE *ss_matlab_load(char *fname) {
  return ss_load(fname);
}

STATE_SPACE *ss_load(char *fname) {
  STATE_SPACE *s=NULL;
  MATRIX *list;
  int i;

#ifdef SDB
  printf("Beginning to load the file %s\n",fname);
#endif

  /* Check if the mlab file exists and has the matrices */
  list = mat_load(fname);
  if (list == NULL) return NULL;
  s = ss_create();
  i = ss_setup(s,list);
  mat_list_free(list);
  if (i < 0) {
    ss_free(s);
    s=NULL;
  }
  return s;
}

int ss_setup(STATE_SPACE *s, MATRIX *list) {
  MATRIX *tmp;

  /* Copy the matrices to the state space equations */
  if ( (tmp = mat_find(list,"A")) == NULL) return -1;  mat_copy(s->A, tmp);
  if ( (tmp = mat_find(list,"B")) == NULL) return -1;  mat_copy(s->B, tmp);
  if ( (tmp = mat_find(list,"C")) == NULL) return -1;  mat_copy(s->C, tmp);
  if ( (tmp = mat_find(list,"D")) == NULL) return -1;  mat_copy(s->D, tmp);

#ifdef SDB
  printf("It has the matrices\n");
#endif
  ss_resize_state(s, mat_rows(s->A));
  ss_resize_inputs(s, mat_columns(s->B));
  ss_resize_outputs(s, mat_rows(s->C));

  if (ss_verify(s) < 0){
    printf("ERROR: matrices are the wrong sizes!\n");
    return -1;
  }
  
#ifdef SDB
  printf("assigned the matrices\n");
#endif

  return 0;
}

STATE_SPACE *ss_create(){
  STATE_SPACE *s;

  s = (STATE_SPACE *)malloc(sizeof(STATE_SPACE));

  if (s==NULL){
    printf(" ss_create error!\n");
    return NULL;
  }

  s->n = 0; s->m = 0; s->p = 0;

  s->A = mat_create();       mat_set_name(s->A, "A");
  s->B = mat_create();       mat_set_name(s->B, "B");
  s->C = mat_create();       mat_set_name(s->C, "C");
  s->D = mat_create();       mat_set_name(s->D, "D");

  s->y   = mat_create();     mat_set_name(s->y, "y");
  s->ycx = mat_create();     mat_set_name(s->ycx, "ycx");
  s->ydu = mat_create();     mat_set_name(s->ydu, "ydu");

  s->u = mat_create();       mat_set_name(s->u, "u");

  s->xcur   = mat_create();  mat_set_name(s->xcur, "xcur");
  s->xnext  = mat_create();  mat_set_name(s->xnext, "xnext");
  s->xdelax = mat_create();  mat_set_name(s->xdelax, "xdelax");
  s->xdelbu = mat_create();  mat_set_name(s->xdelbu, "xdelbu");


  s->namelen = 10;
  strcpy(s->name,"NONAME_SS");

  return s;
}

STATE_SPACE *ss_init(int nstates, int ninputs, int noutputs) {
  STATE_SPACE *s;

  s = ss_create();  if (s==NULL) return NULL;
  ss_resize(s, nstates, ninputs, noutputs);
  return s;
}


int ss_set_name(STATE_SPACE *a, char *name){

  if (a==NULL) return 0;
  
  strcpy(a->name,name);
  a->namelen=strlen(name)+1;
  return 1;
}

char * ss_get_name(STATE_SPACE *a){
  return a->name;
}


void ss_free(STATE_SPACE *s){
  
  if (s==NULL) return;
  
  mat_free(s->A); mat_free(s->B); mat_free(s->C); mat_free(s->D);
  mat_free(s->y); mat_free(s->ycx); mat_free(s->ydu);
  mat_free(s->u);
  mat_free(s->xcur); mat_free(s->xnext);
  mat_free(s->xdelax); mat_free(s->xdelbu);

  free(s);
}

int ss_copy(STATE_SPACE *dst, STATE_SPACE *src) {

  if ((src == NULL) || (dst == NULL)) return 0;
  if (src == dst) return 1;

  mat_copy(dst->A, src->A);
  mat_copy(dst->B, src->B);
  mat_copy(dst->C, src->C);
  mat_copy(dst->D, src->D);
  mat_copy(dst->u, src->u);
  mat_copy(dst->y, src->y);
  mat_copy(dst->ycx, src->ycx);
  mat_copy(dst->ydu, src->ydu);
  mat_copy(dst->xcur, src->xcur);
  mat_copy(dst->xnext, src->xnext);
  mat_copy(dst->xdelax, src->xdelax);
  mat_copy(dst->xdelbu, src->xdelbu);

  return 1;
}


void ss_reset(STATE_SPACE *s) {
  mat_reset(s->y);
  mat_reset(s->u);
  mat_reset(s->xcur);
  mat_reset(s->xnext);
}

double *ss_compute(STATE_SPACE *s, double *input){
  
  ss_set_input(s,input);
  ss_equation(s);
  return ss_output(s);
}

void ss_equation(STATE_SPACE *s) {

  /* Make the next state current so s->xcur = s->xnext = x(k) */
  mat_copy_f(s->xcur, s->xnext);

  /* Calculate and set the current output s->y = y(k) */
  /* y(k) = Cx(k) + Du(k) */
  mat_mult_f(s->ycx,s->C,s->xcur);
  mat_mult_f(s->ydu,s->D,s->u);
  mat_add_f(s->y,s->ycx,s->ydu);


  /* Calculate the next state and place it in s->xnext = x(k+1) */
  /* x(k+1) = Ax(k) + Bu(k) */
  mat_mult_f(s->xdelax,s->A,s->xcur);
  mat_mult_f(s->xdelbu,s->B,s->u);
  mat_add_f(s->xnext,s->xdelax,s->xdelbu);
}


int ss_set_ic(STATE_SPACE *s, double *ic){

  register int i;

  for (i=0;i<s->n;i++,ic++){
    mat_element_set_f(s->xnext,i,0,*ic);
    mat_element_set_f(s->xcur,i,0,*ic);
  }

  return 1;
}


int ss_set_input(STATE_SPACE *s, double *input){
  
  register int i;

  for (i=0;i<s->m;i++,input++)
    mat_element_set_f(s->u,i,0,*input);
  
  return 1;
}

double *ss_output(STATE_SPACE *s){
  return mat_get_real(s->y);
}

double *ss_state(STATE_SPACE *s){
  return mat_get_real(s->xcur);
}

double *ss_nextstate(STATE_SPACE *s){
  return mat_get_real(s->xnext);
}

void ss_print(STATE_SPACE *s) {
  if (s == NULL){
    printf("State Space System Does Not Exist (NULL)!\n");
    return;
  }

  printf("State Space Equation: %i states, %i inputs, %i outputs\n",
	 s->n,s->m,s->p);
  mat_print(s->A);
  mat_print(s->B);
  mat_print(s->C);
  mat_print(s->D);
}

int ss_get_nstates(STATE_SPACE *a){

  if (a==NULL) return -1;
  
  return a->n;
}

int ss_get_ninputs(STATE_SPACE *a){

  if (a==NULL) return -1;
  
  return a->m;
}

int ss_get_noutputs(STATE_SPACE *a){

  if (a==NULL) return -1;
  
  return a->p;
}


MATRIX *ss_el_get(STATE_SPACE *s, char which){

  switch (which){
  case 'a':
  case 'A':
    return s->A;
/*    break; */
  case 'b':
  case 'B':
    return s->B;
/*    break; */
  case 'c':
  case 'C':
    return s->C;
/*    break; */
  case 'd':
  case 'D':
    return s->D;
/*    break; */
  default:
    return NULL;
/*    break; */
  }
}

int ss_el_set(STATE_SPACE *s, char which, MATRIX *mat){

  switch (which){
  case 'a':
  case 'A':
    s->A = mat;
    break;
  case 'b':
  case 'B':
    s->B = mat;
    break;
  case 'c':
  case 'C':
    s->C = mat;
    break;
  case 'd':
  case 'D':
    s->D = mat;
    break;
  default:
    return 0;
/*    break; */
  }
  return 1;
}

int ss_el_free(STATE_SPACE *s, char which){

  switch (which){
  case 'a':
  case 'A':
    if (s->A == NULL) return 1;
    mat_free(s->A);  s->A = NULL;
    break;
  case 'b':
  case 'B':
    if (s->B == NULL) return 1;
    mat_free(s->B);  s->B = NULL;
    break;
  case 'c':
  case 'C':
    if (s->C == NULL) return 1;
    mat_free(s->C);  s->C = NULL;
    break;
  case 'd':
  case 'D':
    if (s->D == NULL) return 1;
    mat_free(s->D);  s->D = NULL;
    break;
  default:
    return 0;
/*    break; */
  }
  return 1;
}

int ss_verify(STATE_SPACE *s){
  if (s->n != mat_get_cols(s->A)) return -1;
  if (s->n != mat_get_rows(s->A)) return -1;
  if (s->m != mat_get_cols(s->B)) return -1;
  if (s->n != mat_get_rows(s->B)) return -1;
  if (s->n != mat_get_cols(s->C)) return -1;
  if (s->p != mat_get_rows(s->C)) return -1;
  if (s->m != mat_get_cols(s->D)) return -1;
  if (s->p != mat_get_rows(s->D)) return -1;
  return 0;
}

int ss_resize_state(STATE_SPACE *s, int nstates){
  s->n = nstates;
  mat_resize(s->xcur,nstates,1); mat_reset(s->xcur);
  mat_resize(s->xnext,nstates,1); mat_reset(s->xnext);
  mat_resize(s->xdelax,nstates,1);
  mat_resize(s->xdelbu,nstates,1);
  return 1;
}

int ss_resize_inputs(STATE_SPACE *s, int ninputs){
  s->m = ninputs;
  mat_resize(s->u,ninputs, 1); mat_reset(s->u);
  return 1;
}

int ss_resize_outputs(STATE_SPACE *s, int noutputs){
  s->p = noutputs;
  mat_resize(s->y,noutputs, 1); mat_reset(s->y);
  mat_resize(s->ycx,noutputs, 1);
  mat_resize(s->ydu,noutputs, 1);
  return 1;
}

int ss_resize(STATE_SPACE *s, int nstates, int ninputs, int noutputs){

  int rval = 0;

  if (s->n != nstates){		/* Incorrect state size */
    rval |= 7;			/* Reset A,B,C */
    ss_resize_state(s, nstates);
  }

  if (s->m != ninputs){		/* Incorrect input size */
    rval |= 10;			/* Reset B,D */
    ss_resize_inputs(s, ninputs);
  }

  if (s->p != noutputs){	/* Incorrect outputs size */
    rval |= 12;			/* Reset C,D */
    ss_resize_outputs(s, noutputs);
  }

  if (rval&0x01){		/* Reset the A matrix */
    mat_resize(s->A, nstates, nstates);
  }
  if (rval&0x02){		/* Reset the B matrix */
    mat_resize(s->B, nstates, ninputs);
  }
  if (rval&0x04){		/* Reset the C matrix */
    mat_resize(s->C, noutputs, nstates);
  }
  if (rval&0x08){		/* Reset the D matrix */
    mat_resize(s->D, noutputs, ninputs);
  }

  if (nstates != mat_get_cols(s->A)) return -1;
  if (nstates != mat_get_rows(s->B)) return -1;
  if (nstates != mat_get_cols(s->C)) return -1;
  if (ninputs != mat_get_cols(s->D)) return -1;
  if (noutputs!= mat_get_rows(s->D)) return -1;

  if (ss_verify(s)<0)
    return -1;

  return rval;
}
