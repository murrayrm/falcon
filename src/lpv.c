/*
 * \file lpv.c 
 * \brief Code for linear parameter varying controller module
 *
 * Written by Michael Kantner.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lpv.h"

LPV *lpv_load(char *fname){
  LPV *l=NULL;
  MATRIX *list;
  int status;

  /* Check if the mlab file exists and has the matrices */
  list = mat_load(fname);
  if (list == NULL) return NULL;
  l=lpv_create();
  status = lpv_setup(l,list);
  mat_list_free(list);
  if (status < 0) {
    lpv_free(l);
    l = NULL;
  }
  return l;
}

int lpv_setup(LPV *l, MATRIX *list){
  MATRIX *temp;

  ss_setup(l->SYS,list);

  if ( (temp = mat_find(list,"DSIZE")) == NULL) return -1;
  mat_copy(l->DSIZE, temp);

  if ( (temp = mat_find(list,"DOFFSET")) == NULL) return -1;
  mat_copy(l->DOFFSET, temp);

  if ( (temp = mat_find(list,"DSCALE")) == NULL) return -1;
  mat_copy(l->DSCALE, temp);

  if (lpv_resize(l) < 0){
    printf("ERROR: lpv matrices are the wrong sizes!\n");
    return -1;
  }

  return 0;
}

LPV *lpv_create(){
  LPV *l=NULL;

  l = (LPV *)malloc(sizeof(LPV));

  if (l==NULL){
    printf(" lpv_create error!\n");
    return NULL;
  }

  l->ndeltas = 0;
  l->sumdeltas = -1;
  l->n = -1;
  l->m = -1;
  l->p = -1;

  l->SYS      = ss_create();  ss_set_name(l->SYS, "State Space");
  l->DSIZE    = mat_create(); mat_set_name(l->DSIZE, "Delta Sizes");
  l->DOFFSET  = mat_create(); mat_set_name(l->DOFFSET, "Delta Offsets");
  l->DSCALE   = mat_create(); mat_set_name(l->DSCALE, "Delta Scales");
  l->deltas   = mat_create(); mat_set_name(l->deltas, "deltas");
  l->u        = mat_create(); mat_set_name(l->u, "u");
  l->y        = mat_create(); mat_set_name(l->y, "y");

  l->namelen = 11;
  strcpy(l->name,"NONAME_LPV");

  return l;
}

int lpv_resize(LPV *l){

  int rval=0;
  int ndeltas = mat_get_rows(l->DSIZE);
  int i;
  int sumdeltas;

  rval = ss_verify(l->SYS);
  if (rval < 0 ) return rval;

  if ( 1 != mat_get_cols(l->DSIZE)) return -1; /*Must be a vector*/

  if (l->ndeltas != ndeltas){
    l->ndeltas = ndeltas;
    mat_resize(l->deltas,ndeltas,1); mat_reset(l->deltas);
    rval |= 8;
  }

  sumdeltas = 0;
  for (i=0; i<ndeltas;i++)
    sumdeltas += (int) mat_element_get(l->DSIZE,i,0);
  if (l->sumdeltas != sumdeltas){
    l->m = mat_get_cols(l->SYS->B) - sumdeltas;
    l->p = mat_get_rows(l->SYS->C) - sumdeltas;
    l->sumdeltas = sumdeltas;
    /* The following is a VERY bad hack.  Needs to be supported. */
    l->u->real = mat_get_real(l->SYS->u) + sumdeltas;
    l->u->ncols = 1;  l->u->nrows = l->m;
    l->y->real = mat_get_real(l->SYS->y) + sumdeltas;
    l->y->ncols = 1;  l->y->nrows = l->p;
    rval |= 16;
  }
  
  return rval;
}

int lpv_set_name(LPV *a, char *name){

  if (a==NULL) return 0;
  
  strcpy(a->name,name);
  a->namelen=strlen(name)+1;
  return 1;
}

char * lpv_get_name(LPV *a){
  return a->name;
}

void lpv_free(LPV *l){

  if (l==NULL) return;

  ss_free(l->SYS);
  mat_free(l->DSIZE);
  mat_free(l->DOFFSET);
  mat_free(l->DSCALE);
  mat_free(l->deltas);

  free(l);
}

double *lpv_compute(LPV *l, double *input, double *delta){

  lpv_set_delta(l,delta);
  lpv_set_input(l,input);
  lpv_equation(l);
  return lpv_output(l);
}

void lpv_equation(LPV *l){ 
  double *dout;
  double val;
  int i,j,cnt;

  /* Create the appropriate input vector based upon the deltas */
  dout = ss_output(l->SYS);  /* Get the most recent outputs */
  cnt = 0 ;
  for(i = 0; i< l->ndeltas; i++){
    for (j=0; j < mat_element_get_f(l->DSIZE,i,0); j++){
      val = mat_element_get_f(l->deltas,i,0) * *dout;
      mat_element_set_f(l->SYS->u,cnt,0,val);
      cnt++;
    }
    if (mat_element_get_f(l->DSIZE,i,0) > 0) dout++;
  }

  ss_equation(l->SYS);
}

int lpv_set_delta(LPV *l, double *input){
  
  register int i;

  for (i=0;i<l->ndeltas;i++,input++)
    mat_element_set_f(l->deltas,i,0,*input);
  
  mat_add_f(l->deltas, l->deltas, l->DOFFSET);
  mat_dotmult_f(l->deltas, l->deltas, l->DSCALE);

#ifdef DELTA_SAT
  for (i=0; i<l->ndeltas;i++){
    if (mat_element_get_f(l->deltas,i,0) > 1.0) 
      mat_element_set_f(l->deltas,i,0,1.0);
    if (mat_element_get_f(l->deltas,i,0) < -1.0) 
      mat_element_set_f(l->deltas,i,0,-1.0);
  }
#endif

  return 1;
}


int lpv_set_input(LPV *l, double *input){
  
  register int i;

  for (i=0;i<l->m;i++,input++)
    mat_element_set_f(l->u,i,0,*input);
  
  return 1;
}

double *lpv_output(LPV *l){
  return mat_get_real(l->y);
}

void lpv_reset(LPV *l){
  ss_reset(l->SYS);
}
