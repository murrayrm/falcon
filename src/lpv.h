/*
 * \file lpv.h
 * \brief Header file for linear parameter varying controller module
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */
   
#ifndef _have_lpv
#define _have_lpv

#ifdef __cplusplus
extern "C"
{
#endif

#include "state.h"

typedef struct lpv{
  int ndeltas;         /* The number of delta operators */
                       /* Some deltas may be of zero size. */
                       /* This is number of rows in DSIZE */
  int sumdeltas;       /* The total size of the delta blocks */
  int n;               /* The number of states  */
  int m;               /* The number of exogenous inputs  */
  int p;               /* The number of exogenous outputs */
                       /* NOTE: m+sumdeltas = columns of SYS->B */

  STATE_SPACE *SYS;    /* The state space system */
  MATRIX      *DSIZE;  /* The size of each delta block */
  MATRIX      *DOFFSET;/* The commands do the following: */
  MATRIX      *DSCALE; /* delta = DSCALE * (deltas + DOFFSET) */

  MATRIX      *deltas; /* The actual deltas (an input vector)  */
  MATRIX      *u;      /* The control inputs */  /*Not malloced*/
  MATRIX      *y;      /* The control outputs */ /*Not malloced*/

  int namelen;    /* name of state space system */
  char name[20];  /* name length (including NULL) */
} LPV;

LPV *lpv_load(char *fname);
int lpv_setup(LPV *l, MATRIX *list);
LPV *lpv_create();
int lpv_resize(LPV *l);
int lpv_set_name(LPV *l, char *name);
char *lpv_get_name(LPV *l);
void lpv_free(LPV *l);

double *lpv_compute(LPV *l, double *input, double *delta);
void lpv_equation(LPV *l);
int lpv_set_delta(LPV *l, double *delta);
int lpv_set_input(LPV *l, double *input);
double *lpv_output(LPV *l);
void lpv_reset(LPV *l);

#ifdef __cplusplus
}
#endif

#endif
