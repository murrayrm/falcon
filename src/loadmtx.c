/*
 * loadmtx - C language routine to load a Falcon style matrix from 
 *    an ASCII file MAT-file.
 *
 * The 'loadmtx' routine returns 0 if the read is successful and 1 if
 * and end-of-file or read error is encountered.  'loadmtx' can be called
 * repeatedly until the file is exhausted and an EOF is encountered.
 *
 * Return values:
 *    1 = EOF
 *    0 = OK
 *   -1 = Error
 * 
 * Author Michael Kantner, 8-14-95
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "matrix.h"

/* #define LM_DEBUG */

int loadmtx(FILE *fp, int *type, int *mrows, int *ncols, 
            int *imagf, char *pname, double **preal, double **pimag)
{
  int mn, namelen, i, j;
  double dtype, drows, dcols, dimagf;
  double temp;
  char tempname[256];

  /*
   * Get the matrix size/type information.
   */

#ifdef LM_DEBUG
  printf("    Entering loadmtx.c\n");
#endif

  /* Read the type of the matrix */
  if (fscanf(fp,"%s",tempname)           !=1) return -1;
  if (fscanf(fp,"%lf",&dtype)            !=1) return -1;
  if (fscanf(fp,"%lf %lf",&drows,&dcols) !=2) return -1;
  if (fscanf(fp,"%lf",&dimagf)           !=1) return -1;

  *type   = (int) dtype;
  *mrows  = (int) drows;
  *ncols  = (int) dcols;
  *imagf  = (int) dimagf;
  namelen = strlen(tempname)+1;
  strncpy(pname,tempname,MATRIX_NAMELEN);
  pname[MATRIX_NAMELEN]='\0';

#ifdef LM_DEBUG
  printf("  Name (from file): %s\n", tempname);
  printf("  Name (in Falcon): %s\n", pname);
  printf("  Info:  Type of matrix is %i\n",*type);
  printf("    Rows: %i, Cols: %i, imag: %i, namelen: %i\n",*mrows,*ncols,
	 *imagf,namelen);
  fflush(stdout);
#endif

  mn = *mrows * *ncols;

  /*
   * Get Real part of matrix from file
   */
#ifdef LM_DEBUG
  printf("    In loadmtx.c: getting real part.\n");
#endif

  if (mn <= 0){  /* No data for the matrix! */
    printf("Warning:  loadmtx.c: Null matrix\n");
    *preal=NULL;
    *pimag=NULL;
    return 0;
  }

  /*  So we now know that the matrix has elements */
  if ((*preal = (double *) malloc(mn*sizeof(double)))==NULL) {
    printf("\nError: Variable too big to load\n");
    return -1;
  }

#ifdef LM_DEBUG
  printf("    In loadmtx.c: malloced the real part.\n");
#endif

  for(i=0; i<*mrows; i++)
    for(j=0; j<*ncols; j++){

#ifdef LM_DEBUG
    printf("entry %3i,%3i: ",i,j);
#endif

    if (fscanf(fp,"%lf",&temp) != 1) {
      printf("\nError: Failed to read real part of matrix\n");
      free(*preal); *preal=NULL; return -1;
    }


    *(*preal + j*(*mrows) + i) = temp;

#ifdef LM_DEBUG
    printf("%12.4g\n",*(*preal + j*(*mrows) + i));
#endif
  }

#ifdef LM_DEBUG  
  printf("    In loadmtx.c: got real part\n");
#endif

  /* If there is no imaginary part, we are done! */
  if (*imagf == 0) return 0;

  /* Otherwise get the imaginary part */
  if ((*pimag = (double *) malloc(mn*sizeof(double)))==NULL) {
    printf("\nError: Variable too big to load\n");
    free(*preal);preal = NULL;
    return -1;
  }

  for(i=0; i<*mrows; i++)
    for(j=0; j<*ncols; j++){

#ifdef LM_DEBUG
      printf("entry %3i,%3i: ",i,j);
#endif

      if (fscanf(fp,"%lf",&temp) != 1) {
	printf("\nError: Failed to read real part of matrix\n");
	free(*preal); *preal=NULL; return -1;
      }
      
      
      *(*pimag + j*(*mrows) + i) = temp;

#ifdef LM_DEBUG
      printf("%12.4g\n",*(*pimag + j*(*mrows) + i));
#endif
  }

#ifdef LM_DEBUG
  printf("    In loadmtx.c: got imag part (if it exists)\n");
#endif
  
  return(0);
}
