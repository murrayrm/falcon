/*
 * \file pid.c
 * \brief PID controller implementation
 *
 * \author Richard M. Murray
 * \date 6 Jul 08
 *
 * Basic implementation of a PID controller, including anti-windup
 * compensation.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <assert.h>
#include <strings.h>
#include "pid.h"

/*
 * Initialization routes - use to set up Kalman filter
 *
 * pid_create()	create PID data structure
 * pid_init()	initialize PID structure
 * pid_setup()	set up PID from matrix list
 * pid_load()	load PID from file
 *
 */

/* Create the Kalman filter structure */
PID_CONTROL *pid_create() {
  PID_CONTROL *c;

  /* Allocate and initialize the basic data structure */
  c = (PID_CONTROL *) calloc(1, sizeof(PID_CONTROL));
  assert(c != NULL);

  /* Initialize individual gains */
  c->kp = c->ki = c->kp = 0; 		/* basic gains */
  c->kt = 0;				/* antiwindup */
  c->beta = c->gamma = 1;		/* setpoint weighting */

  return c;
}

/* Setup/update the matrices that define the Kalman filter */
int pid_setup(PID_CONTROL *c, double kp, double ki, double kd) {
  /* Save the controller gains */
  c->kp = kp;
  c->ki = ki;
  c->kd = kd;

  /* Mark the filter as initialized */
  c->initialized = 1;
  return 0;
}

PID_CONTROL *pid_init(double kp, double ki, double kd) {
  PID_CONTROL *c;

  c = pid_create(); assert(c != NULL);
  pid_setup(c, kp, ki, kd);
  return c;
}

/* Set the basic PID gains */
int pid_set_gains(PID_CONTROL *c, double kp, double ki, double kd) {
  return pid_setup(c, kp, ki, kd);
}

/* Set the setpoint weights */
int pid_set_weights(PID_CONTROL *c, double beta, double gamma) {
  c->beta = beta;
  c->gamma = gamma;
  return 0;
}

/* Set the anti-windup gain */
int pid_set_awgain(PID_CONTROL *c, double kt) {
  c->kt = kt;
  return 0;
}

int pid_free(PID_CONTROL *c)
{
# warning pid_free not implemented
  return 0;
}

/*
 * PID controller actions
 *
 */

/* Update a PID controller with antiwindup and discrete time dynamics */
double pid_compute_aw(PID_CONTROL *c, double r, double y, 
		      double uact, double *inp)
{
  double u, der;

  /* Update the integral error, including antiwindup compensation */
  c->integral += (r - y) - c->kt * (c->last_inp - uact);

  /* Compute the (setpoint weighted) derivative */
  der = (c->gamma * r - y) - c->last_err;
  c->last_err = (c->gamma * r - y);

  /* Compute the control */
  u = c->kp * (c->beta*r - y) + c->ki * c->integral + c->kd * der;

  /* Store the control in the user supplied pointer */
  if (inp != NULL) *inp = u;

  /* Return the estimate for easy access */
  return u;
}

/* Update a PID controller based on discrete time dynamics */
double pid_compute(PID_CONTROL *c, double r, double y, double *inp)
{
  return pid_compute_aw(c, r, y, c->last_inp, inp);
}

