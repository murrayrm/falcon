/*
 * \file lutable.c 
 * \brief Lookup table functions
 *
 * Written by Michael Kantner.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <stdlib.h>		/* for size_t */
#include "lutable.h"

#undef LU_DEBUG

LU_DATA *lu_load(char *file) {
  FILE *fp;
  double doub1, doub2, doub3, doub4;  /* temp variables */
  int i,j;
  char c;
  LU_DATA *l = NULL;

#ifdef LU_DEBUG
  printf("  Starting to load the LU data\n");
#endif

  if ((fp = fopen(file, "r")) == NULL) { /* If the does not exist */
    return NULL;
  }

#ifdef LU_DEBUG
  printf("  Opened the lookup table file\n");
#endif

  /* Read in the dimension of the table and number of outputs */
  fscanf(fp, "%lf %lf %lf",&doub1,&doub2,&doub3);
  while ((c = getc(fp)) != EOF && c != '\n') ; /*read until end of line*/

  l=lu_create((int) doub1);  /*Allocate the structure*/
  if (l == NULL) {fclose(fp); return NULL;}
  l->nout = (int) doub2;
  l->ninterp = (int) doub3;
#ifdef LU_DEBUG
  printf("  Table is %i dimensional with %i outputs.\n",l->ndim,l->nout);
#endif

  /* Get information for each dimension */
  for (i=0; i<l->ndim; i++){
    fscanf(fp, "%lf %lf %lf %lf",&doub1,&doub2,&doub3,&doub4);
    l->info[i].low    = doub1;
    l->info[i].inc    = doub2;
    l->info[i].high   = doub3;
    l->info[i].nsteps = (int) doub4;
    while ((c = getc(fp)) != EOF && c != '\n') ; /*read until end of line*/
#ifdef LU_DEBUG
    printf("Dim %i:  low=%g, inc=%g, hi=%g, n=%i.\n",i,l->info[i].low,
	   l->info[i].inc,l->info[i].high,l->info[i].nsteps);
#endif
  }

  i=lu_resize(l);
  if (i<0) {lu_free(l);fclose(fp);return NULL;}

#ifdef LU_DEBUG
  printf("  Allocated the LU structure.\n");
#endif

  /* Read the file */
  for (i = 0; i < l->elements; i++){
    for (j = 0; j < l->ndim; j++){
      fscanf(fp, "%lf", &doub1);  /* Read in the coord value */
      *(*(l->coords + i) + j) = doub1;
    }

    for (j = 0; j < l->nout; j++){
      fscanf(fp, "%lf", &doub1 );
      *(*(l->data + i) + j) = doub1;
    }

    while ((c = getc(fp)) != EOF && c != '\n') ; /*read until end of line*/
  }

  fclose(fp);

#ifdef LU_DEBUG
  printf("  Closed the table file.\n");
#endif

  return l;
}

LU_DATA *lu_create(int ndim){
  LU_DATA *l=NULL;
  int i;

  l = (LU_DATA *) malloc(sizeof(LU_DATA));
  if (l==NULL) return NULL;

  l->ndim = ndim;
  l->nout = 0;
  l->elements = 0;

  l->info = (LU_INFO *) malloc(sizeof(LU_INFO) * ndim);
  if (l->info == NULL) {lu_free(l);return NULL;}
  for (i=0; i<ndim; i++){
    l->info[i].low = 0.0;    l->info[i].high = 0.0;
    l->info[i].inc = 0.0;    l->info[i].nsteps = 0;
  }

  l->offsets = (int *) malloc(ndim *sizeof(int));
  l->base    = (int *) malloc(ndim *sizeof(int));
  l->outside = (int *) malloc(ndim *sizeof(int));

  l->coords = NULL; l->data = NULL;

  return l;
}

int lu_resize(LU_DATA *l){
  int i,j;
  double *temp;
  
  if (l == NULL) return (-1);
  /* ASSERT: l->info is initialized correctly */
  /* Calculate the offset vector and number of elements*/

  j = 1;
  for (i = l->ndim-1; i>=0; i--){
    l->offsets[i]=j;
    j=j*l->info[i].nsteps;
#ifdef LU_DEBUG
    printf("Offset %i: %i\n",i,l->offsets[i]);
#endif
  }

  l->elements = l->offsets[0] * l->info[0].nsteps;

  /*  Now allocate space for all of the data and coordinates */
#ifdef LU_DEBUG
  printf("Allocating coords and data (%d * 16 bytes)\n", l->elements);
#endif
  l->coords = (double **) malloc((size_t) l->elements*sizeof(double *));
  l->data   = (double **) malloc((size_t) l->elements*sizeof(double *));
  if ( (l->coords==NULL) || (l->data==NULL) ) return(-1);

  for (i = 0; i < l->elements; i++){
    temp = (double *) malloc((size_t) l->ndim*sizeof(double));
    if (temp==NULL) return (-2);
    l->coords[i]=temp;
    temp = (double *) malloc((size_t) l->nout*sizeof(double));
    if (temp==NULL) return (-2);
    l->data[i]=temp;
  }

  return 0;
}
#undef LU_DEBUG


int lu_lookup(LU_DATA *l, double *coord, double *data) { 
  /* Lookup a given value in the table*/
  register int i,j;
  int retval = 0;
  double diff;
  double **cornerdataloc = l->data;
  double *cornerdata, *otherdata;
  double *lcoord = coord;       /* local pointers to the arguments */
  double *ldata  = data;
  int *base      = l->base;     /* local pointers to the structure */
  int *outside   = l->outside;
  LU_INFO *info  = l->info;
  int *offsets   = l->offsets;

  /* Get position in the lookup table */
  /* Note:  many variables are preinitialized for this loop */
  for (i=0; i<l->ndim; i++, base++, outside++, info++, offsets++, lcoord++){
    *base = (int) ((*lcoord - info->low)/info->inc);
    /* restrict to domain of the table */
    if (*base < 0 ) {
      *base    = 0; 
      *outside = 1;
    } 
    else if (*base >= info->nsteps - 1){
      *base    = info->nsteps - 1;
      *outside = 1;
    }
    else *outside = 0;

    cornerdataloc += *base * (*offsets);

#ifdef LU_DEBUG
    printf("Loop Index %i: Base %i, Outside %i\n",i,*base,*outside);
#endif
  }
  
  /* Initialize the result to the lower corner */
  for (i=0,cornerdata=*cornerdataloc; i < l->nout; i++,ldata++,cornerdata++){
    *ldata = *cornerdata;
  }

  /* Linear Interpolate */
  for (i=0, outside=l->outside, info=l->info, offsets=l->offsets, 
       base=l->base, lcoord = coord; 
       i < l->ndim;
       i++, outside++, info++, offsets++, base++, lcoord++){ 

    if (*outside){/* Don't interpolate off the table */
      retval = 1;
    }
    else {
      /* Calc. slope in each dimension */
      /* Pretty Form:
       * diff = (*lcoord - (info->low+info->inc*(*base)) )/ info->inc;
       * Fast Form: */
      diff = (*lcoord - info->low)/info->inc - *base;

#ifdef LU_DEBUG
      printf("Coord %i difference: %g (%g %g %g)\n",i,diff,*lcoord,
	     (info->low+info->inc*(*base)),info->inc);
#endif

      for (j=0,ldata=data,
	   otherdata=*(cornerdataloc+*offsets), cornerdata=*cornerdataloc;
	   j< l->ninterp; 
	   j++,ldata++,otherdata++,cornerdata++){
	*ldata += diff*( *otherdata - *cornerdata);
      }

    }
  }
 
  return retval;
}


void lu_free(LU_DATA *l) {
  int i;

  if (l == NULL) return;
  
  if (l->coords != NULL)
    for(i=0; i < l->elements; i++) free(l->coords[i]);
  free(l->coords);

  if (l->data != NULL)
    for(i=0; i < l->elements; i++) free(l->data[i]);
  free(l->data);

  free(l->info);
  free(l->offsets);
  free(l->base);
  free(l->outside);

  free(l);

}

