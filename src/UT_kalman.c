/*
 * \file UT_kalman.c
 * \brief Kalman filter unit test program
 *
 * \author Richard M. Murray
 * \date 5 Mar 08
 *
 */

#include "kalman.h"

int main(int argc, char **argv)
{
  /* Load up a Kalman filter */
  KALMAN_FILTER *kf = kf_load(argv[1]);

  /* Allocate space for inputs and measurements */
  double *inp = calloc(kf->ninputs, sizeof(double));
  double *meas = calloc(kf->noutputs, sizeof(double));

  mat_print(kf->P);
  kf_compute_continuous(kf, inp, meas, 0.1, NULL);
  mat_print(kf->dP);

  return 0;
}
