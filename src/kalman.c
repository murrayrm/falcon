/*
 * \file kalman.c
 * \brief Simple Kalman filter implementation
 *
 * \author Richard M. Murray
 * \date 4 Mar 08
 *
 * Basic implementation of a Kalman filter.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <assert.h>
#include <strings.h>

#include "kalman.h"

/*
 * Initialization routes - use to set up Kalman filter
 *
 * kf_create()	create KF data structure
 * kf_init()	initialize KF structure
 * kf_setup()	set up KF from matrix list
 * kf_load()	load KF from file
 *
 */

/* Create the Kalman filter structure */
KALMAN_FILTER *kf_create() {
  KALMAN_FILTER *f;

  /* Allocate and initialize the basic data structure */
  f = (KALMAN_FILTER *) calloc(1, sizeof(KALMAN_FILTER));
  assert(f != NULL);

  f->ninputs = f->noutputs = f->nstates = f->ndisturb = 0;

  /* Allocate space for inidivual matrices and data vectors */
  f->A = mat_create();       mat_set_name(f->A, "A");
  f->B = mat_create();       mat_set_name(f->B, "B");
  f->C = mat_create();       mat_set_name(f->C, "C");
  f->D = mat_create();       mat_set_name(f->D, "D");
  f->F = mat_create();       mat_set_name(f->F, "F");

  /* Matrices for the estimation process */
  f->Rv = mat_create();      mat_set_name(f->Rv, "Rv");
  f->Rw = mat_create();      mat_set_name(f->Rw, "Rw");
  f->P = mat_create();       mat_set_name(f->P, "P");
  f->L = mat_create();       mat_set_name(f->L, "L");
  f->xhat = mat_create();    mat_set_name(f->xhat, "xhat");

  /* Matrices that we use during computations */
  f->dP = mat_create();      mat_set_name(f->dP, "dP");
  f->dxhat = mat_create();   mat_set_name(f->dxhat, "dxhat");
  f->yerr = mat_create();    mat_set_name(f->yerr, "yerr");
  f->Ct_Rw_inv = mat_create();   mat_set_name(f->Ct_Rw_inv, "Ct_Rw_inv");
  f->Ct_Rw_inv_C = mat_create(); mat_set_name(f->Ct_Rw_inv_C, "Ct_Rw_inv_C");

  /* Set the matrix name */
  strcpy(f->name, "NONAME_KF");
  f->namelen = strlen(f->name);

  return f;
}

/* Setup/update the matrices that define the Kalman filter */
int kf_setup(KALMAN_FILTER *f, MATRIX *list) {
  MATRIX *tmp;

  /* Load matrices that determine the dimensions of the filter */
  if ((tmp = mat_find(list, "A")) != NULL) mat_copy(f->A, tmp);
  if ((tmp = mat_find(list, "B")) != NULL) mat_copy(f->B, tmp);
  if ((tmp = mat_find(list, "F")) != NULL) mat_copy(f->F, tmp);
  if ((tmp = mat_find(list, "C")) != NULL) mat_copy(f->C, tmp);

  /* Set up the size of filter */
  kf_resize(f, f->B->nrows, f->B->ncols, f->C->nrows, f->F->ncols);
  assert(f->nstates != 0 && f->noutputs != 0);

  /* Make sure that everything is the right size */
  assert(f->A->nrows == f->nstates && f->A->ncols == f->nstates);
  assert(f->B->nrows == f->nstates && f->B->ncols == f->ninputs);
  assert(f->C->nrows == f->noutputs && f->C->ncols == f->nstates);
  assert(f->F->nrows == f->nstates && f->F->ncols == f->ndisturb);

  /* Now load the other matrices that we need */
  if ((tmp = mat_find(list, "D")) != NULL) mat_copy(f->D, tmp);
  assert(f->D->nrows == f->noutputs && f->D->ncols == f->ninputs);

  if ((tmp = mat_find(list, "Rv")) != NULL) mat_copy(f->Rv, tmp);
  assert(f->Rv->nrows == f->ndisturb && f->Rv->ncols == f->ndisturb);

  if ((tmp = mat_find(list, "Rw")) != NULL) mat_copy(f->Rw, tmp);
  assert(f->Rw->nrows == f->noutputs && f->Rw->ncols == f->noutputs);

  /* Optional matrices that can be specified to give initial conditions */
  if ((tmp = mat_find(list, "X0")) != NULL) mat_copy(f->xhat, tmp);
  if (f->xhat->real == NULL) mat_resize(f->xhat, f->nstates, 1);
  assert(f->xhat->real != NULL);

  if ((tmp = mat_find(list, "P0")) != NULL) mat_copy(f->P, tmp);
  if (f->P->real == NULL) mat_resize(f->P, f->nstates, f->nstates);
  assert(f->P->real != NULL);

  /* Create matrices required for internal operations */
  if (f->L->real == NULL) mat_resize(f->L, f->nstates, f->noutputs);
  assert(f->L->real != NULL);

  if (f->dP->real == NULL) mat_resize(f->dP, f->nstates, f->nstates);
  assert(f->dP->real != NULL);

  if (f->dxhat->real == NULL) mat_resize(f->dxhat, f->nstates, 1);
  assert(f->dxhat->real != NULL);

  if (f->yerr->real == NULL) mat_resize(f->yerr, f->noutputs, 1);
  assert(f->yerr->real != NULL);

  if (f->Ct_Rw_inv->real == NULL)
    mat_resize(f->Ct_Rw_inv, f->nstates, f->noutputs);
  assert(f->Ct_Rw_inv->real != NULL);

  if (f->Ct_Rw_inv_C->real == NULL)
    mat_resize(f->Ct_Rw_inv_C, f->nstates, f->noutputs);
  assert(f->Ct_Rw_inv_C->real != NULL);

  /* Update any intermediate computations that we need */
  MATRIX *tmp1 = mat_create(), *tmp2 = mat_create(); 
  int status;
  status = mat_transpose(tmp1, f->C); assert(status == 0);
  status = mat_inverse(tmp2, f->Rw); assert(status == 0);
  status = mat_mult(f->Ct_Rw_inv, tmp1, tmp2); assert(status == 0);
  status = mat_mult(f->Ct_Rw_inv_C, f->Ct_Rw_inv, f->C); assert(status == 0);
  mat_free(tmp1); mat_free(tmp2);

  /* Print the information out */
  mat_print(f->Ct_Rw_inv);
  mat_print(f->Ct_Rw_inv_C);

  /* Makr the filter as initialized */
  f->initialized = 1;
  return 0;
}

KALMAN_FILTER *kf_init(int nstates, int ninputs, int noutputs, int ndisturb) {
  KALMAN_FILTER *f;

  f = kf_create(); assert(f != NULL);
  kf_resize(f, nstates, ninputs, noutputs, ndisturb);
  return f;
}

KALMAN_FILTER *kf_load(char *fname) {
  KALMAN_FILTER *f = NULL;
  MATRIX *list;
  int i;

  /* Check if the mlab file exists and has the matrices */
  list = mat_load(fname);
  if (list == NULL) return NULL;

  f = kf_create();
  i = kf_setup(f, list);
  mat_list_free(list);
  if (i < 0) {
    kf_free(f);
    f=NULL;
  }
  return f;
}

int kf_resize(KALMAN_FILTER *f, int nstates, int ninputs, int noutputs,
	  int ndisturb) {
  f->nstates = nstates;
  f->ninputs = ninputs;
  f->noutputs = noutputs;
  f->ndisturb = ndisturb;

  return 0;
}

int kf_free(KALMAN_FILTER *f)
{
# warning kf_free not implemented
  return 0;
}

/*
 * Kalman filter actions
 *
 */

/* Update a Kalman filter based on continuous time dynamics */
double *kf_compute_continuous(KALMAN_FILTER *f, double *inp,
			      double *meas, double dT, double *der)
{
  register int i, j, k, l;

  /* Make sure arguments look OK */
  assert(f != NULL && f->initialized);
  assert(meas != NULL);

  /* Set up pointers to the data locations for the various matrices */
  int nr = f->nstates;
  double *P = f->P->real, *dP = f->dP->real;
  double *A = f->A->real, *B = f->B->real, *C = f->C->real, *F = f->F->real;
  double *Rv = f->Rv->real, *Ct_Rw_inv = f->Ct_Rw_inv->real;
  double *Ct_Rw_inv_C = f->Ct_Rw_inv_C->real, *L = f->L->real;
  double *xhat = f->xhat->real, *dxhat = f->dxhat->real, *yerr = f->yerr->real;

  /* Compute dP */
  for (i = 0; i < nr; ++i) {
    for (j = 0; j < nr; ++j) {
      /* Initialized elements to zero */
      dP[i + j*nr] = 0;

      /* Update based on state dynamics */
      for (k = 0; k < nr; ++k)
	dP[i + j*nr] += A[i + k*nr] * P[k + j*nr] + P[i + k*nr] * A[j + k*nr];

      /* Add in the disturbance terms: F Rv F^T  */
      int nd = f->ndisturb;
      for (k = 0; k < nd; ++k)
	for (l = 0; l < nd; ++l)
	  dP[i + j*nr] += F[i + k*nr] * Rv[k + l*nd] * F[j + l*nr];

      /* Finally, add in the correction term: P C Rw^{-1} C^T P */
      for (k = 0; k < nr; ++k)
	for (l = 0; l < nr; ++l)
	  dP[i + j*nr] -= P[i + k*nr] * Ct_Rw_inv_C[k + l*nr] * P[j + l*nr];
    }
  }

  /* Update the covariance */
  for (i = 0; i < nr; ++i)
    for (j = 0; j < nr; ++j) 
      P[i + j*nr] += dP[i + j*nr] * dT;

  /* Compute the optimal feedback gain */
  for (i = 0; i < nr; ++i) {
    int no = f->noutputs;
    for (j = 0; j < no; ++j) {
      L[i + j*nr] = 0;
      for (k = 0; k < nr; ++k)
	L[i + j*nr] += P[i + k*nr] * Ct_Rw_inv[k + j*nr];
    }
  }

  /* Compute the output error */
  int no = f->noutputs;
  for (i = 0; i < no; ++i) {
    yerr[i] = meas[i];
    for (j = 0; j < nr; ++j) yerr[i] -= C[i + j*no] * xhat[j];
  }

  /* Compute dx */
  for (i = 0; i < nr; ++i) {
    /* Reset the increment */
    dxhat[i] = 0;

    /* Update based on dynamics */
    if (der != NULL) {
      /* Use the derivative update that was passed to us */
      for (j = 0; j < nr; ++j) dxhat[i] += der[i];

    } else {
      /* Use the linear dynamics */
      for (j = 0; j < nr; ++j) dxhat[i] += A[i + j*nr] * xhat[j];
    }

    /* Add inputs (user can set B = 0 if this is included in der) */
    if (inp != NULL)
      for (j = 0; j < f->ninputs; ++j) dxhat[i] += B[i + j*nr] * inp[j];

    /* Add in correction term */
    for (j = 0; j < no; ++j) dxhat[i] += L[i + j*nr] * yerr[j];
  }    

  /* Propogate the state */
  for (i = 0; i < nr; ++i) xhat[i] += dxhat[i] * dT;

  /* Return the estimate for easy access */
  return xhat;
}

