/*
 * \file kalman.h
 * \brief Header file for Kalman filter module
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#ifndef __falcon_kalman__
#define __falcon_kalman__

#include "matrix.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* Continuous time Kalman filter structure */
typedef struct kalman_filter {
  int nstates;				/* Number of states (for system) */
  int ninputs;				/* Number of inputs (for system) */
  int noutputs;				/* Number of outputs (measurements) */
  int ndisturb;				/* Number of process disturbances */

  MATRIX *A, *B, *C, *D, *F;		/* State space description */
  MATRIX *xhat, *P;			/* Estimatation variables */
  MATRIX *Rv, *Rw, *L;			/* Covariances and gain matrices */

  /* Tempororary variables used internal to code */
  MATRIX *dP, *dxhat, *yerr;		/* updates for Kalman filter */
  MATRIX *Ct_Rw_inv, *Ct_Rw_inv_C;	/* pre-multiplied matrices */

  int namelen;				/* Name length (including NULL) */
  char name[20];			/* Name of the filter */
  int initialized;			/* Set if properly initalized */
} KALMAN_FILTER;

KALMAN_FILTER *kf_init(int nstates, int ninputs, int noutputs, int ndisturbs);
KALMAN_FILTER *kf_create();
int kf_setup(KALMAN_FILTER *f, MATRIX *list);
KALMAN_FILTER *kf_load(char *);
int kf_resize(KALMAN_FILTER *, int nstates, int ninputs, int noutputs,
              int ndisturbs);
int kf_free(KALMAN_FILTER *);

int kf_set_initial(KALMAN_FILTER *f, double *xinit, double **Pinit);
int kf_set_dynamics(KALMAN_FILTER *f, double **A, double **B, double **C);

double *kf_compute_continuous(KALMAN_FILTER *f, double *inp, double *meas,
			      double dT, double *der);

#ifdef __cplusplus
}
#endif
#endif
