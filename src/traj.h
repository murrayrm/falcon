/* 
 * \file traj.h
 * \brief the main trajectory tracking routines
 *
 * \author Michael Kantner
 *
 * Trajectories are optimized 1-d lookup tables.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#ifndef __TRAJ_INCLUDED__
#define __TRAJ_INCLUDED__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct trajectory_data {
  double *time;
  double **data;
  int current;
  int nrows;			/* The number of time steps */
  int ncols;			/* The number of outputs */
  int flag;
} TRAJ_DATA;

#define TRAJF_BADTIME   0x0001

/* Creation and sizing */
TRAJ_DATA *traj_create(void);
int traj_resize(TRAJ_DATA *trjp, int rows, int cols);
void traj_free(TRAJ_DATA *trjp);

/* Automated creation */
TRAJ_DATA *traj_init(int rows, int cols);
TRAJ_DATA *traj_load(char *file);


int traj_reset(TRAJ_DATA *trjp);
int traj_setcurrent(TRAJ_DATA *trjp, int row);
int traj_read(TRAJ_DATA *trjp, double *vector, double time);
int traj_rate(TRAJ_DATA *trjp, double *vector, double time);

void traj_disp(TRAJ_DATA *trjp);

/* Accessor fucntions */
int traj_outputs(TRAJ_DATA *trjp);
int traj_rows(TRAJ_DATA *trjp);
int traj_current(TRAJ_DATA *trjp);
int traj_status(TRAJ_DATA *trjp);
int traj_row(TRAJ_DATA *trjp, int row, double *vector, double *other);

/* Changing individual rows */
int traj_setrow(TRAJ_DATA *trjp, int row, double time, double *data);

/* Save a trajectory to disk */
int traj_write(TRAJ_DATA *trjp, char *filename);

/*
 * Old function names, kept for posterity
 */
int traj_numcols(TRAJ_DATA *trjp);
int traj_numrows(TRAJ_DATA *trjp);
int traj_getcurrent(TRAJ_DATA *trjp);
int traj_datachange(TRAJ_DATA *trjp, double time, double *data, int row);
int traj_table(TRAJ_DATA *trjp, double *vector, int index);
int traj_table2(TRAJ_DATA *trjp, double *vector, double *other, int index);
int traj_rowset(TRAJ_DATA *trjp, int row);


#ifdef __cplusplus
}
#endif

#endif /* __TRAJ_INCLUDED__ */
