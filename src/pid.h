/*
 * \file pid.h
 * \brief PID controller implementation
 *
 * \author Richard M. Murray
 * \date 6 Jul 08
 *
 * Basic implementation of a PID controller, including anti-windup
 * compensation.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#ifndef __falcon_pid__
#define __falcon_pid__

#include "matrix.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* Continuous time PID controller structure */
typedef struct pid_control {
  double kp, ki, kd, kt;		/* Integrator gains */

  double integral, last_err, last_inp;	/* PID states */

  int spweight_flag;			/* Set point weighting flag */
  double beta, gamma;			/* Set point weights */

  int namelen;				/* Name length (including NULL) */
  char name[20];			/* Name of the filter */
  int initialized;			/* Set if properly initalized */
} PID_CONTROL;

PID_CONTROL *pid_init(double kp, double ki, double kd);
PID_CONTROL *pid_create();
int pid_setup(PID_CONTROL *f, double kp, double ki, double kd);
int pid_free(PID_CONTROL *);

double pid_compute(PID_CONTROL *f, double r, double y, double *inp);
double pid_compute_aw(PID_CONTROL *f, double r, double y, 
		      double uact, double *inp);

#ifdef __cplusplus
}
#endif
#endif
