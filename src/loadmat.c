/*
 * loadmat - C language routine to load a matrix from a MAT-file.
 *
 * * Here is an example that uses 'loadmat' to load a matrix from a MAT-file:
 *
 *	FILE *fp;
 *	char name[20];
 *	int type, mrows, ncols, imagf;
 *	double *xr, *xi;
 *	fp = fopen("foo.mat","rb");
 *	loadmat(fp, &type, name, &mrows, &ncols, &imagf, &xr, &xi);
 *      fclose(fp);
 *	free(xr);
 *	if (imagf) free(xi);
 *
 * The 'loadmat' routine returns 0 if the read is successful and 1 if
 * and end-of-file or read error is encountered.  'loadmat' can be called
 * repeatedly until the file is exhausted and an EOF is encountered.
 *
 * Return values:
 *    1 = EOF
 *    0 = OK
 *   -1 = Error
 * 
 * Works for level 1.0 mat-files, as described in the external reference guide.
 *
 * Author Michael Kantner, 2-21-95
 * Based loosely upon work by Eric Wemhoff in July 1994
 * Uses the calling format of loadmat.c, an old matlab routine.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_MATIO
#include <matio.h>
#include <string.h>
#else
typedef void mat_t;
#endif

int loadmat(mat_t* mat, int *type, int *mrows, int *ncols,
	    int *imagf, char *pname, double **preal, double **pimag) {
#ifdef HAVE_MATIO
  int i;

  /* Read the next matrix in the file */
  matvar_t* var = Mat_VarReadNext(mat);
	
  if(var == NULL) {
/*  printf("Header: %s\n", mat->header); */
    return -1;
  }

  /* Make sure the matrix is the right dimensions */
  if (var->rank > 2) {
    printf("\nError: More than 2 dimensions in the matrix\n");
    return -1;
  }

  /* Save the data */
  *mrows = var->dims[0];
  *ncols = var->dims[1];
  *imagf = var->isComplex;
  strcpy(pname, var->name);

  /* Copy the data */
  int mn = *mrows * *ncols;

  if (mn <= 0){  /* No data for the matrix! */
    printf("Warning:  loadmat.c: Null matrix\n");
    *preal=NULL;
    *pimag=NULL;
    return 0;
  }

  /*  So we now know that the matrix has elements */
  if ((*preal = (double *) malloc(mn*sizeof(double)))==NULL) {
    printf("\nError: Variable too big to load\n");
    return -1;
  }

  for(i=0; i<mn; i++){
#ifdef LM_DEBUG
    printf("entry %3i: ",i);
#endif
    *(*preal + i) = ((double*)(var->data))[i];

#ifdef LM_DEBUG
    printf("%12.4g\n",*(*preal + i));
#endif
  }

#ifdef LM_DEBUG  
  printf("    In loadmat.c: got real part\n");
#endif

  /* Print out information about the matrix */
  printf("Loaded: %s, %s\n", pname, var->name);
  Mat_VarPrint(var, 1);
  printf("Yep\n");

  return 0;
#else
  fprintf(stderr, "loadmat not implemented\n");
  return -1;
#endif
}
