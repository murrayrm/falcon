/*
 * \file lutable.h
 * \brief Lookup table functions
 *
 * Written by Michael Kantner.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#ifndef __LUTABLE_INCLUDED__
#define __LUTABLE_INCLUDED__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct lu_info {
  double low;      /* The low value */
  double inc;      /* The step size */
  double high;     /* The high value */
  int    nsteps;   /* redundant, but handy. */
} LU_INFO;

typedef struct lu_data {
  int    ndim;       /* Dimension of the lookup table */
  int    nout;       /* Number of output data fields  */
  int    ninterp;    /* Number of outputs to interpolate */
                     /* The first ninterp of nout are interpolated */
  int    elements;   /* Total number of elements in the list */
  struct lu_info *info;
  double **coords;
  double **data;
  int *offsets;   /* Number that must be skipped for an increment */
  int *base;      /* Working array, lower corner */
  int *outside;   /* Working array, are we within table bounds? */
} LU_DATA;


LU_DATA *lu_load(char *file);
LU_DATA *lu_create(int ndim);
int lu_resize(LU_DATA *l);
int lu_lookup(LU_DATA *table, double *coord, double *data);
void lu_free(LU_DATA *table);
void lu_dispinfo(LU_DATA *l);
int lu_setrange(LU_DATA *l, int index, double low, double inc, double high,
		int steps);
int lu_setoutput(LU_DATA *l, int output, int interp);
int lu_setcoord(LU_DATA *l);
int lu_write(LU_DATA *l, char *file);

int     lu_setel(LU_DATA *l, int row, int col, double value);
double  lu_getel(LU_DATA *l, int row, int col);
double *lu_getrow(LU_DATA *l, int row);

/* Accessor functions */
int lu_dimension(LU_DATA *l);
int lu_outputs(LU_DATA *l);
int lu_interp(LU_DATA *l);
int lu_elements(LU_DATA *l);
int lu_coordinate(LU_DATA *l, int row, double *coord);

#ifdef __cplusplus
}
#endif

#endif  /* __LUTABLE_INCLUDED__ */
