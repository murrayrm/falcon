/*
 * \file lutable3.c 
 * \brief additional ookup table functions
 *
 * Written by Michael Kantner.
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include <stdio.h>
#include "lutable.h"

#define LU_DEBUG

/*
 * Accessor functions for the lookup table.
 */

int lu_dimension(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->ndim;
}

int lu_outputs(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->nout;
}

int lu_interp(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->ninterp;
}

int lu_elements(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->elements;
}

int lu_coordinate(LU_DATA *l, int row, double *coord){
  int i;

  if (l == (LU_DATA *) 0) return -1;

  if (row >= l->elements) return -2;  /* Out of range */

  for (i=0; i < lu_dimension(l); i++){
    *(coord + i) = *( *(l->coords + row) + i);
  }

  return 0;
}  
 
