/*
 * \file mat_mult.c 
 * \brief general purpose matrix routines
 *
 * \date August 1995
 * \author Michael Kantner
 *
 * Copyright (c) 2008 by California Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the California Institute of Technology nor
 *    the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CALTECH
 * OR THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $Id$
 */

#include "matrix.h"

int mat_mult(MATRIX *dst, MATRIX *a, MATRIX *b) { /* dst = a.b */
  MATRIX *a1, *b1;
  int freea1=0;
  int freeb1=0;

  if ((dst == (MATRIX *)0) || (a == (MATRIX *)0) || (b == (MATRIX *)0)) return -1;
  if (a->ncols != b->nrows) return -1;

  if (dst == a){
    a1 = mat_create();
    mat_copy(a1,a);
    freea1 = 1;
  }
  else
    a1 = a;

  if (dst == b){
    b1 = mat_create();
    mat_copy(b1,b);
    freeb1 = 1;
  }
  else
    b1 = b;

  mat_resize(dst,a1->nrows,b1->ncols);

  mat_mult_f(dst,a1,b1);

  if (freea1) mat_free(a1);
  if (freeb1) mat_free(b1);
  
  return 0;
}

void mat_mult_f(MATRIX *dst, MATRIX *a, MATRIX *b) { /* dst = a.b */

  register int i,j,k;

  mat_reset(dst);

  for (i = 0; i < dst->nrows; i++)
    for (j = 0; j < dst->ncols; j++)
      for (k = 0; k < a->ncols; k++)
	*(dst->real + i + j*dst->nrows) += 
	  *(a->real + i + k*a->nrows ) * *(b->real + k + j*b->nrows);

}

